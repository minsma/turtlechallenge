﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TurtleChallenge
{
    class Turtle
    {
        int position_x;
        int position_y;
        string moveDirection;

        public Turtle(int pos_x, int pos_y, string movDir)
        {
            position_x = pos_x;
            position_y = pos_y;
            moveDirection = movDir;
        }

        public (int, int) GetPosition()
        {
            return (position_x, position_y);
        }

        public string GetMoveDirection()
        {
            return moveDirection;
        }

        public void ChangeDirection(string newDirection)
        {
            moveDirection = newDirection;
        }

        public void ChangePosition((int, int) nextPos)
        {
            position_x = nextPos.Item1;
            position_y = nextPos.Item2;
        }
    }

    class Mine
    {
        int position_x;
        int position_y;

        public Mine(int pos_x, int pos_y)
        {
            position_x = pos_x;
            position_y = pos_y;
        }

        public (int, int) GetPosition()
        {
            return (position_x, position_y);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ((int,int) boardSize, (int,int) gameEndPos, List<Mine> mines, Turtle turtle) = ReadGameSettings("game-settings");

            MainFunction("moves", boardSize, gameEndPos, turtle, mines);
        }

        static ((int, int), (int, int), List<Mine>, Turtle) ReadGameSettings(string fileName)
        {
            var fileLines = File.ReadAllLines(fileName + ".txt");

            var firstLineSplited = fileLines[0].Split(" ");
            (int, int) boardSize = (Convert.ToInt32(firstLineSplited[0]), 
                                    Convert.ToInt32(firstLineSplited[1]));

            var secondLineSplited = fileLines[1].Split(" ");
            Turtle turtle = new Turtle(Convert.ToInt32(secondLineSplited[0]),
                                       Convert.ToInt32(secondLineSplited[1]),
                                       secondLineSplited[2]);

            var thirdLineSplited = fileLines[2].Split(" ");
            (int, int) gameEndPos = (Convert.ToInt32(thirdLineSplited[0]), 
                                     Convert.ToInt32(thirdLineSplited[1]));

            List<Mine> mines = new List<Mine>();

            for (int i = 3; i < fileLines.Length; i++)
            {
                var lineSplited = fileLines[i].Split(" ");
                Mine mine = new Mine(Convert.ToInt32(lineSplited[0]), Convert.ToInt32(lineSplited[1]));
                mines.Add(mine);
            }

            return (boardSize, gameEndPos, mines, turtle);
        }

        static void MainFunction(string fileName, (int, int) boardSize, (int, int) gameEnd, Turtle turtle, List<Mine> mines)
        {
            StreamReader file = new StreamReader(fileName + ".txt");
            string line;
            int count = 1;

            while ((line = file.ReadLine()) != null)
            {
                var values = line.Split(" ");

                if (values.Length == 1)
                {
                    turtle.ChangeDirection(values[0]);
                    Console.WriteLine("Sequence {0}: Success", count++);
                }
                else
                {
                    (int, int) nextPos = (Convert.ToInt32(values[0]), Convert.ToInt32(values[1]));

                    if (DoesCanMove(turtle.GetPosition(), turtle.GetMoveDirection(), nextPos, boardSize))
                    {
                        if(DoesHitMine(nextPos, mines))
                        {
                            Console.WriteLine("Sequence {0}: Mine hit!", count++);
                        }
                        else
                        {
                            Console.WriteLine("Sequence {0}: Success", count++);
                        }

                        turtle.ChangePosition(nextPos);
                    }
                    else
                    {
                        Console.WriteLine("Sequence {0}: Turtle can not move", count++);
                    }
                }
            }

            if (turtle.GetPosition().Item1 != gameEnd.Item1 && turtle.GetPosition().Item2 != gameEnd.Item2)
            {
                Console.WriteLine("Turtle not finished successfully");
            }
            else
            {
                Console.WriteLine("Turtle finished successfully");
            }
        }

        static bool DoesHitMine((int, int) nextPos, List<Mine> mines)
        {
            foreach (var mine in mines)
            {
                if (mine.GetPosition().Item1 == nextPos.Item1 && mine.GetPosition().Item2 == nextPos.Item2)
                {
                    return true;
                }
            }

            return false;
        }

        static bool DoesCanMove((int, int) turtlePos, string turtleDirection, (int, int) nextPos, (int, int) boardSize)
        {
            if (turtleDirection == "North" || turtleDirection == "South")
            {
                if (Math.Abs(turtlePos.Item2 - nextPos.Item2) == 1 && turtlePos.Item1 == nextPos.Item1 &&
                             nextPos.Item2 < boardSize.Item2 && nextPos.Item2 > 0)
                {
                    return true;
                }

                return false;
            }
            else if (turtleDirection == "East" || turtleDirection == "West")
            {
                if (Math.Abs(turtlePos.Item1 - nextPos.Item1) == 1 && turtlePos.Item2 == nextPos.Item2 &&
                             nextPos.Item1 < boardSize.Item1 && nextPos.Item1 > 0)
                {
                    return true;
                }

                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
